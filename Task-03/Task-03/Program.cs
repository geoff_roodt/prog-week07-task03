﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            var myNumber = 0;
            Console.WriteLine("Enter a number:");
            myNumber = int.Parse(Console.ReadLine());

            BasicCalculations(myNumber);
        }

        static void BasicCalculations(int number)
        {
            Console.WriteLine($"{number} + {number} = {number + number}");
            Console.WriteLine($"{number} - {number} = {number - number}");
            Console.WriteLine($"{number} / {number} = {number / number}");
            Console.WriteLine($"{number} * {number} = {number * number}");
        }
    }
}
